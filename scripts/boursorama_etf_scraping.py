#!/usr/bin/env python
# coding: utf-8

import datetime
import pandas as pd
from bs4 import BeautifulSoup
import requests
import re


pea_etf_link_list = ["https://www.boursorama.com/bourse/trackers/recherche/autres/page-1?beginnerEtfSearch%5BisEtf%5D=1&beginnerEtfSearch%5Btaxation%5D=1",
"https://www.boursorama.com/bourse/trackers/recherche/autres/page-2?beginnerEtfSearch%5BisEtf%5D=1&beginnerEtfSearch%5Btaxation%5D=1",
"https://www.boursorama.com/bourse/trackers/recherche/autres/page-3?beginnerEtfSearch%5BisEtf%5D=1&beginnerEtfSearch%5Btaxation%5D=1",
"https://www.boursorama.com/bourse/trackers/recherche/autres/page-4?beginnerEtfSearch%5BisEtf%5D=1&beginnerEtfSearch%5Btaxation%5D=1",
"https://www.boursorama.com/bourse/trackers/recherche/autres/page-5?beginnerEtfSearch%5BisEtf%5D=1&beginnerEtfSearch%5Btaxation%5D=1",
"https://www.boursorama.com/bourse/trackers/recherche/autres/page-6?beginnerEtfSearch%5BisEtf%5D=1&beginnerEtfSearch%5Btaxation%5D=1",
]

link_list = []
for pea_page in pea_etf_link_list:
    page = requests.get(pea_page)
    soup = BeautifulSoup(page.content, 'html.parser')
    a = soup.find_all("a", {"class": "c-link c-link--animated"})
    for e in a:
        link = re.search(r'href="([^"]+)"', str(e))
        if link:
            link_list.append(link.group(1))


isin_list = []
etf_list = []
for link in link_list:
    full_link = f"https://www.boursorama.com{link}"
    page = requests.get(full_link)
    soup = BeautifulSoup(page.content, 'html.parser')
    h2 = soup.find_all("h2", {"class": "c-faceplate__isin"})
    if len(h2) > 0:
        isin = h2[0].text.split(' ')[0].rstrip(' ')
        isin_list.append(isin)
    a = soup.find_all("a", {"class": "c-faceplate__company-link"})
    if len(a) > 0:
        etf = a[0].text.lstrip(" \n").rstrip(" \n")
        etf_list.append(etf)


assets = {"isin": isin_list, "company": etf_list}

df = pd.DataFrame.from_dict(assets)
df.to_csv("assets.csv", index=False)




