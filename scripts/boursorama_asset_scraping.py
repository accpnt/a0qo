#!/usr/bin/env python
# coding: utf-8

import datetime
import pandas as pd
from bs4 import BeautifulSoup
import requests
import re


pea_link_list = ["https://www.boursorama.com/bourse/actions/cotations/page-1?quotation_az_filter[market]=SRD&quotation_az_filter[letter]=&quotation_az_filter[peaEligibility]=1&quotation_az_filter[filter]=", 
                 "https://www.boursorama.com/bourse/actions/cotations/page-2?quotation_az_filter[market]=SRD&quotation_az_filter[letter]=&quotation_az_filter[peaEligibility]=1&quotation_az_filter[filter]=", 
                 "https://www.boursorama.com/bourse/actions/cotations/page-3?quotation_az_filter[market]=SRD&quotation_az_filter[letter]=&quotation_az_filter[peaEligibility]=1&quotation_az_filter[filter]=",
                 "https://www.boursorama.com/bourse/actions/cotations/page-4?quotation_az_filter[market]=SRD&quotation_az_filter[letter]=&quotation_az_filter[peaEligibility]=1&quotation_az_filter[filter]=",
                 "https://www.boursorama.com/bourse/actions/cotations/page-5?quotation_az_filter[market]=SRD&quotation_az_filter[letter]=&quotation_az_filter[peaEligibility]=1&quotation_az_filter[filter]=",
                 "https://www.boursorama.com/bourse/actions/cotations/page-6?quotation_az_filter[market]=SRD&quotation_az_filter[letter]=&quotation_az_filter[peaEligibility]=1&quotation_az_filter[filter]=",
                 "https://www.boursorama.com/bourse/actions/cotations/page-7?quotation_az_filter[market]=SRD&quotation_az_filter[letter]=&quotation_az_filter[peaEligibility]=1&quotation_az_filter[filter]=",
                 "https://www.boursorama.com/bourse/actions/cotations/page-8?quotation_az_filter[market]=SRD&quotation_az_filter[letter]=&quotation_az_filter[peaEligibility]=1&quotation_az_filter[filter]="]


data_ist_list = []
for pea_page in pea_link_list:
    page = requests.get(pea_page)
    soup = BeautifulSoup(page.content, 'html.parser')
    trs = soup.find_all("tr", {"class": "c-table__row"})
    for tr in trs:
        data_ist = re.search(r'data-ist="([^"]+)"', str(tr))
        if data_ist:
            data_ist_list.append(data_ist.group(1))
print(data_ist_list)


isin_list = []
company_list = []
for data_ist in data_ist_list:
    link = f"https://www.boursorama.com/cours/{data_ist}"
    page = requests.get(link)
    soup = BeautifulSoup(page.content, 'html.parser')
    h2 = soup.find_all("h2", {"class": "c-faceplate__isin"})
    isin = h2[0].text.split(' ')[0].rstrip(' ')
    isin_list.append(isin)
    a = soup.find_all("a", {"class": "c-faceplate__company-link"})
    company = a[0].text.lstrip(" \n").rstrip(" \n")
    company_list.append(company)


assets = {"isin": isin_list, "company": company_list}

df = pd.DataFrame.from_dict(assets)
df.to_csv("assets.csv", index=False)




