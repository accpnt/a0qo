import datetime
import argparse
import numpy as np
import pandas as pd
import yfinance as yf
import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

def load_isin_dataset(isin_csv_file):
    data = pd.read_csv(isin_csv_file, sep=';')
    if "radiation" in data.columns:
        mask = data["radiation"].isnull()
        filtered = data[mask]
    else:
        filtered = data

    isin = filtered[["isin", "company"]]

    return isin


def extract_indicator(df, indicator:str):
    if df.empty:
        i = df
    else:
        i = df[df['index'] == indicator]
        i = i.set_index('index').T
        i.reset_index(inplace=True)
        try:
            i.columns = ["Date", indicator]
        except:
            i = df
    
    return i

def compute_epsg(df):
    df["epsg"] = np.nan
    try:
        for index, row in df.iterrows():
            if index == 0:
                continue
            else:
                df.at[index, 'epsg'] = 100*((df.at[index, 'eps']/df.at[index-1, 'eps'])-1)
        df["epsg"] = df["epsg"].round(2)
    except:
        logging.error("error computing epsg")

    return df


def compute_indicators(isin_ticker):
    balance_sheet = isin_ticker.balance_sheet.reset_index()
    income_statement = isin_ticker.income_stmt.reset_index()

    result = np.nan

    if balance_sheet.shape[1] == income_statement.shape[1]:
        # variables of interest from balance sheet (iloc helps us remove "index" column)

        equity = extract_indicator(balance_sheet, "Stockholders Equity")
        common_stock = extract_indicator(balance_sheet, "Common Stock")

        # variables of interest from income statement (iloc helps us remove "index" column)
        profit = extract_indicator(income_statement, "Gross Profit")
        revenue = extract_indicator(income_statement, "Total Revenue")
        net_income = extract_indicator(income_statement, "Net Income")

        # compute return on equity
        try:
            roe = pd.merge(net_income, equity, how="left", on="Date")
            roe["roe"] = roe["Net Income"] / roe["Stockholders Equity"] * 100
            roe["roe"] = roe["roe"].round(2)

            eps = pd.merge(profit, common_stock, how="left", on="Date")
            eps["eps"] = eps["Gross Profit"] / eps["Common Stock"]
            eps["eps"] = eps["eps"].round(2)

            merged = pd.merge(roe, eps, how="left", on="Date")

            merged['date'] = pd.to_datetime(merged['Date'])
            sorted = merged.sort_values(by='date')
            sorted = sorted.reset_index(drop=True)
            result = compute_epsg(sorted)

        except: 
            roe = np.nan
            eps = np.nan
            epsg = np.nan
            result = np.nan

    else: 
        logging.warning("invalid shapes between balance and income")

    return result


def isin_ticker_loop(data, threshold, output_file):
    count_missing = 0

    shortlist = []

    # Retrieve data using yfinance library
    for index, row in data.iterrows():
        isin = row["isin"]
        company = row["company"]
        isin_ticker = yf.Ticker(isin)

        indicators = compute_indicators(isin_ticker)

        if indicators is not np.nan:
            if indicators["roe"].iloc[-1] > threshold and indicators["eps"].iloc[-1] > threshold:
                logging.info(row["company"], "with" , isin, "meets the criteria")
                indicators["isin"] = isin
                indicators["company"] = company
                shortlist.append(indicators)
        else:
            count_missing += 1

    result = pd.concat(shortlist, axis=0)
    result = result[['isin', 'company', 'date', 'roe', 'eps']]
    result.to_csv(output_file, index=False)
    
    return count_missing


def main():
    parser = argparse.ArgumentParser(description="ROE/EPS/EPSG indicators computation")
    
    # Add command-line arguments
    parser.add_argument("input_file", help="Path to the input file")
    parser.add_argument("output_file", help="Path to the output file")
    parser.add_argument("threshold", help="Required threshold for ROE/EPS")
    
    args = parser.parse_args()
    
    # Access the command-line arguments and flags
    input_file = args.input_file
    output_file = args.output_file
    threshold = args.threshold

    isin_dataset = load_isin_dataset(input_file)
    count_missing = isin_ticker_loop(isin_dataset, float(threshold), output_file)

    logging.info(count_missing, "/", isin_dataset.shape[0], "indicators haven't been computed due to missing data")

if __name__ == "__main__":
    main()
