import pandas as pd
import plotly.express as px

df_oat3y = pd.read_csv("../data/oat/FRABENCHMARK3A_2024-11-14.txt", sep="\t")
df_oat10y = pd.read_csv("../data/oat/FRABENCHMARK10A_2024-11-14.txt", sep="\t")

df_merge = pd.merge(df_oat10y, df_oat3y, on='date', how='inner')

df_merge["diff_clot"] = df_merge["clot_x"] - df_merge["clot_y"]
print(df_merge)

fig = px.line(df_merge, x="date", y="diff_clot")
fig.show()

