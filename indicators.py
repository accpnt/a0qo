import yfinance as yf
import pandas as pd
import numpy as np
from scipy.stats import linregress
import argparse
import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

def load_isin_dataset(isin_csv_file):
    data = pd.read_csv(isin_csv_file, sep=';')
    if "radiation" in data.columns:
        mask = data["radiation"].isnull()
        filtered = data[mask]
    else:
        filtered = data

    isin = filtered[["isin", "company"]]

    return isin


def compute_indicators(isin_ticker):
    data = isin_ticker.history(period="max")

    # Adjust Close prices for dividends
    data['Adj Close'] = data['Close'] + data['Dividends'].cumsum()

    # Compute daily returns including dividends
    data['Total Return'] = data['Adj Close'].pct_change()

    # Compute the cumulative total return
    cumulative_total_return = (1 + data['Total Return']).cumprod()

    # Compute the total return over the entire period
    total_return = cumulative_total_return.iloc[-1] - 1
    data['Total Return'] = total_return
    print(f"Total Return: {total_return}")

    # NAV (assuming 'Close' represents NAV for simplicity)
    nav = data['Close']
    print(f"Latest NAV: {nav.iloc[-1]}")


    # Sharpe Ratio
    risk_free_rate = 0.01
    annual_return = data['Total Return'].mean() * 252
    annual_std = data['Total Return'].std() * np.sqrt(252)
    sharpe_ratio = (annual_return - risk_free_rate) / annual_std
    print(f"Sharpe Ratio: {sharpe_ratio}")

    # Dividend Yield
    annual_dividends = data['Dividends'].sum()
    latest_price = data['Close'].iloc[-1]
    dividend_yield = annual_dividends / latest_price
    print(f"Dividend Yield: {dividend_yield}")

    # Liquidity Measures
    average_daily_volume = data['Volume'].mean()
    print(f"Average Daily Volume: {average_daily_volume}")

    # Volatility
    volatility = data['Total Return'].std() * np.sqrt(252)
    print(f"Volatility: {volatility}")

    # Sortino Ratio
    downside_returns = data[data['Total Return'] < 0]['Total Return']
    downside_std = downside_returns.std() * np.sqrt(252)
    sortino_ratio = (annual_return - risk_free_rate) / downside_std
    print(f"Sortino Ratio: {sortino_ratio}")


def isin_ticker_loop(data, threshold, output_file):
    count_missing = 0

    shortlist = []

    # Retrieve data using yfinance library
    for index, row in data.iterrows():
        isin = row["isin"]
        company = row["company"]

        try:
            print(isin)
            isin_ticker = yf.Ticker(isin)
            compute_indicators(isin_ticker)
        except:
            print("No data for {isin}")


def main():
    parser = argparse.ArgumentParser(description="ETF performance indicators computation")
    
    # Add command-line arguments
    parser.add_argument("input_file", help="Path to the input file")
    parser.add_argument("output_file", help="Path to the output file")
    parser.add_argument("threshold", help="Required threshold for Sharpe ratio")
    
    args = parser.parse_args()
    
    # Access the command-line arguments and flags
    input_file = args.input_file
    output_file = args.output_file
    threshold = args.threshold

    isin_dataset = load_isin_dataset(input_file)
    count_missing = isin_ticker_loop(isin_dataset, float(threshold), output_file)

    logging.info(count_missing, "/", isin_dataset.shape[0], "indicators haven't been computed due to missing data")

if __name__ == "__main__":
    main()