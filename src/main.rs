use polars::prelude::*;
use std::env;
use std::error::Error;
use plotly::{Plot, Scatter};

fn main() -> Result<(), Box<dyn Error>> {
    // Collect command-line arguments
    let args: Vec<String> = env::args().collect();

    // Check if the correct number of arguments is provided
    if args.len() != 3 {
        println!("Usage: {} <oat10.txt> <oat3.txt>", args[0]);
        std::process::exit(1);
    }

    let path_oat10y = args[1].clone();
    let path_oat3y = args[2].clone();

    
    // Load DataFrames from the provided CSV file paths
    let df_oat10y = CsvReadOptions::default()
        .with_infer_schema_length(None)
        .with_has_header(true)
        .with_parse_options(CsvParseOptions::default().with_try_parse_dates(true).with_separator(b'\t'))
        .try_into_reader_with_file_path(Some(path_oat10y.into()))?
        .finish()?;

    let df_oat3y = CsvReadOptions::default()
        .with_infer_schema_length(None)
        .with_has_header(true)
        .with_parse_options(CsvParseOptions::default().with_try_parse_dates(true).with_separator(b'\t'))
        .try_into_reader_with_file_path(Some(path_oat3y.into()))?
        .finish()?;

    // Perform an inner join on the "date" column
    let joined_df = df_oat10y.inner_join(&df_oat3y, ["date"], ["date"])?;

    let result = joined_df
    .clone()
    .lazy()
    .select([
        (col("date")).alias("date"),
        (col("clot") - col("clot_right")).alias("diff")
    ])
    .collect()?;
    println!("{}", result);

    let dates = result
            .column("date").unwrap()
            .datetime().unwrap()
            .to_vec();

    let values = result
        .column("diff").unwrap()
        .f64().unwrap()
        .to_vec();

    let trace = Scatter::new(dates, values)
        .name("Yield curve OAT");

    let mut plot = Plot::new();
    plot.add_trace(trace);
    plot.show();

    Ok(())
}
